package org.dbpedia.spotlight.bean

class DBpediaResource(score:Double, resource:String ) {

  def getScore:Double = score

  def getURI:String = resource

  def getURL:String = "http://dbpedia.org/resource/".concat(resource)

}
