package org.ner.sparql.utils

import java.net.URL
import java.net.HttpURLConnection
import java.io.OutputStreamWriter
import java.io.BufferedReader
import java.io.InputStreamReader
import scala.util.control.Breaks._
import java.util.Properties
import java.net.URLEncoder
import scala._
import java.util

class SparqlUtils {

  val SPARQL_QUERY = "SELECT DISTINCT ?syn WHERE { {?disPage dbpedia-owl:wikiPageDisambiguates <http://dbpedia.org/resource/%s> .  ?disPage dbpedia-owl:wikiPageDisambiguates ?syn .  }  UNION  { <http://dbpedia.org/resource/%s> dbpedia-owl:wikiPageDisambiguates ?syn . }}"

  def getContent(endpoint: String, urlParameters: String): String = {

    val url: URL = new URL(endpoint)
    val connection: HttpURLConnection = url.openConnection().asInstanceOf[HttpURLConnection]
    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
    connection.setRequestProperty("Accept", "application/json")
    connection.setRequestProperty("Request-Method", "POST")
    connection.setRequestProperty("Accept-Charset", "utf-8")
    connection.setRequestProperty("Connection", "keep-alive")
    connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length))
    connection.setRequestMethod("POST")
    connection.setInstanceFollowRedirects(false)
    connection.setUseCaches(false)

    connection.setDoOutput(true)
    connection.setDoInput(true)
    connection.connect()

    val bufferWriter: OutputStreamWriter = new OutputStreamWriter(connection.getOutputStream)
    bufferWriter.write(urlParameters)
    bufferWriter.flush()

    val bufferReader: BufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream))

    val buffer: StringBuilder = new StringBuilder
    var line: String = ""

    while (bufferReader.ready()) {
      line = bufferReader.readLine()
      if (line == null) break()
      buffer.append(line)
    }

    bufferReader.close()
    bufferWriter.close()

    buffer.toString()

  }

  def getUrlParameters(text: String, encoding: String): String = {

    val parameters: Properties = new Properties()

    parameters.setProperty("query", URLEncoder.encode(text, encoding))
    parameters.setProperty("default-graph-uri", URLEncoder.encode("http://dbpedia.org", encoding))
    parameters.setProperty("timeout", "30000")
    parameters.setProperty("debug", "on")

    val iterator: util.Iterator[AnyRef] = parameters.keySet().iterator()

    var first: Int = 0

    val buffer: StringBuilder = new StringBuilder

    while (iterator.hasNext) {
      val name: String = iterator.next().asInstanceOf[String]
      val value: String = parameters.getProperty(name)

      if (first != 0)
        buffer.append("&")

      buffer.append(name)
      buffer.append("=")
      buffer.append(value)

      first += 1

    }

    buffer.toString()

  }

}
