package org.ner

import scala.io.Source
import org.apache.commons.lang.WordUtils
import java.io.{PrintStream, File}
import org.ner.sparql.utils.SparqlUtils
import net.liftweb.json.JsonParser._
import org.dbpedia.spotlight.utils.FileUtils._
import org.dbpedia.spotlight.bean.DBpediaResource
import scala.collection.mutable.ArrayBuffer
import org.apache.commons.logging.LogFactory
import java.net.URL

object Converter {

  private val sparql = new SparqlUtils
  private val LOG = LogFactory.getLog(Converter.getClass)

  implicit val formats = net.liftweb.json.DefaultFormats


  def getOficialPage(uri: String): String = {

    if (uri.contains("_(disambiguation)")) return ""

    try {

      val con = new URL("http://live.dbpedia.org/resource/%s".format(uri)).openConnection()

      con.connect()

      val is = con.getInputStream()

      val resultURL = con.getURL().toString

      is.close()

      return resultURL

    } catch {
      case ioe: Exception => return ""
    }

  }

  def main(args: Array[String]) {


    if (args.length < 2) {
      println("\nUsage: Converter <ner result dir>  <dictionary>  \n")
      System.exit(1)
    }

    val current_dir = args(0)
    val dictionary = Source.fromFile(args(1), "UTF-8").getLines().mkString("\n").split("\n")

    val files = new File(current_dir).listFiles.filter(_.getName.contains(".log"))

    for (file <- files) {


      LOG.info("Reading file: %s ...".format(file.getName))

      val text = Source.fromFile(file, "UTF-8").mkString

      val array = text.split(" ")

      val result = array.filterNot(_.contains("/O")).distinct

      var buffer = new ArrayBuffer[DBpediaResource]

      LOG.info("Trying to retrieve DBpedia resources...")

      result.foreach(e => {
        val uri = WordUtils.capitalizeFully(e.split("/")(0))

        if (uri.trim.size != 0) {
          val result = sparql.getContent("http://dbpedia.org/sparql", sparql.getUrlParameters(sparql.SPARQL_QUERY.format(uri, uri), "UTF-8"))
          val json = parse(result) \ "results" \ "bindings" \ "syn"

          json.children.foreach(sri => {

            val uri = (sri.children.head \ "value").extractOrElse[String]("").replace("http://dbpedia.org/resource/", "")

            val oficialURI = getOficialPage(uri).replace("http://live.dbpedia.org/page/", "")

            if (dictionary.contains(oficialURI) && buffer.filter(_.getURI.equals(oficialURI)).isEmpty)
                buffer.append(new DBpediaResource(0, oficialURI))

            if (dictionary.contains(uri) && buffer.filter(_.getURI.equals(oficialURI)).isEmpty)
              buffer.append(new DBpediaResource(0, uri))

          })
        }
      })

      LOG.info("Writing file: %s ...".format(file.getName.replace(".log", "")))

      val allOut = new PrintStream("../results/ner/%s.json".format(file.getName.replace(".log", "")))

      append(allOut, buffer.toList.sortBy(-_.getScore))

      allOut.close()

    }


  }


}
