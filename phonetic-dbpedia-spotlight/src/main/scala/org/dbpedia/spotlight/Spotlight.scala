package org.dbpedia.spotlight

import java.io.{File, PrintStream}
import org.dbpedia.spotlight.bean.DBpediaResource
import scala.io.Source
import org.dbpedia.spotlight.utils.DBpediaSpotlightUtils._
import net.liftweb.json.JsonParser._
import scala.collection.mutable.ArrayBuffer
import org.dbpedia.spotlight.utils.FileUtils._
import org.apache.commons.logging.LogFactory
import org.apache.commons.lang.WordUtils


object Spotlight {

  implicit val formats = net.liftweb.json.DefaultFormats

  private val LOG = LogFactory.getLog(Spotlight.getClass)

  def main(args: Array[String]) {


    if (args.length < 3) {
      println("\nUsage: Spotlight <Spotlight Service> <Texts folder> <dictionar>y  \n")
      System.exit(1)
    }


    val spotlight_endpoint = args(0)
    val current_dir = args(1)
    val dictionary = args(2)

    for (file <- new File(current_dir).listFiles) {

      LOG.info("Processing file: %s ...".format(file.getAbsoluteFile))

      val text = WordUtils.capitalizeFully(("now witnessed history as told by the people who would that and an american president campbell's on a historic trip to communist china its nineteen seventy two china still are in the tightly controlled communist mold of chairman mao is close to the west in los but on this gray when today party officials have gathered at the capital's apple to that doll kovac that spot and against the cold and very unusual this attack is about to ride a series of readings segment as severe force one when president nixon in the rest of us on that have landed in beijing airport would spend all it was on the plane as special assistant to us national security advice that henry kissinger mrs. informatics midyear political event of chorus and therefore i think we sort of expected derrick ,comma for airport ceremony would monitor or naive factors was very spared day was a great day anyway fairways are small band and i cried and some people put up deion that day was a very restrained but not on friendly bridge as snow ,comma are no jubilation in no sense of history which is appropriate when you look back on it in terms of two countries that haven't even been talk of each other for over twenty years watching president nixon as he gulped down the steps of the presidential plane and weak to chinese penny as joe alight with a droll smiled with generates joan bans those about had a fifty sixty feet away blinder and of the group to offer inclusion pulled the local residents for press but wanting to remember is that to do a taste of how it felt that it the chinese of coolest control exactly with enough force one clock will be permitted peaking of port than they did it three"))

      println(getContent(spotlight_endpoint, getUrlParameters(text, "UTF-8")))

      /*val json = parse(getContent(spotlight_endpoint, getUrlParameters(text, "UTF-8")))

      val resources = json \\ "surfaceForm" \\ "resource"

      var buffer = new ArrayBuffer[DBpediaResource]

      resources.children.foreach(resource => {

        val top = resource.children.head

        val uri = if ((top \\ "@uri").children.isEmpty) (top \\ "@uri").extractOrElse[String]("") else (top \\ "@uri").children.head.extractOrElse[String]("")

        val contextualScore = if ((top \\ "@contextualScore").children.isEmpty) (top \\ "@contextualScore").extractOrElse[String]("0").toDouble else ((top \\ "@contextualScore").children.head.extractOrElse[String]("0")).toDouble

        if (!uri.equals("") && buffer.filter(_.getURI.equals(uri)).isEmpty && dictionary.contains(uri))
          buffer.append(new DBpediaResource(contextualScore, uri))
      })

      val allOut = new PrintStream("../results/phonetic-dbpedia-spotlight/%s.json".format(file.getName.replace(".txt", "")))

      LOG.info("Writing file: %s ...".format(file.getName.replace(".txt", "")))

      append(allOut, buffer.toList.sortBy(-_.getScore))

      allOut.close()  */

    }

  }


}
