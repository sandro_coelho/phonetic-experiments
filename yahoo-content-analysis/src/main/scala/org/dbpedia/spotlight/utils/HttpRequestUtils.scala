package org.dbpedia.spotlight.utils

import java.net.URL
import java.net.HttpURLConnection
import java.io.OutputStreamWriter
import java.io.BufferedReader
import java.io.InputStreamReader
import scala.util.control.Breaks._
import java.util.Properties
import java.net.URLEncoder
import scala._
import java.util

object HttpRequestUtils {

  /**
   *
   * Execute a  request using URL Parameters
   *
   * @param endpoint
   * @param urlParameters
   * @param accept
   * @return
   */
  def getContent(endpoint: String, urlParameters: String, accept:String = "application/json"): String = {

    val url: URL = new URL(endpoint)
    val connection: HttpURLConnection = url.openConnection().asInstanceOf[HttpURLConnection]
    connection.setRequestProperty("Content-Type", "application/x-www-form-urlencoded")
    connection.setRequestProperty("Accept", accept)
    connection.setRequestProperty("Request-Method", "POST")
    connection.setRequestProperty("Accept-Charset", "utf-8")
    connection.setRequestProperty("Connection", "keep-alive")
    connection.setRequestProperty("Content-Length", "" + Integer.toString(urlParameters.getBytes().length))
    connection.setRequestMethod("POST")
    connection.setInstanceFollowRedirects(false)
    connection.setUseCaches(false)
    

    connection.setDoOutput(true)
    connection.setDoInput(true)
    connection.connect()

    val bufferWriter: OutputStreamWriter = new OutputStreamWriter(connection.getOutputStream)
    bufferWriter.write(urlParameters)
    bufferWriter.flush()

    val bufferReader: BufferedReader = new BufferedReader(new InputStreamReader(connection.getInputStream))

    val buffer: StringBuilder = new StringBuilder
    var line: String = ""

    while (bufferReader.ready()) {
      line = bufferReader.readLine()
      if (line == null) break()
      buffer.append(line)
    }

    bufferReader.close()
    bufferWriter.close()

    buffer.toString()

  }

  /**
   *
   *  Returns formatted URL
   *
   * @param text
   * @param encoding
   * @return
   */
  def getUrlParameters(text: String, encoding: String): String = {

    val parameters: Properties = new Properties()

    parameters.setProperty("q", URLEncoder.encode("select * from contentanalysis.analyze where text='%s'".format(text), encoding))


    val iterator: util.Iterator[AnyRef] = parameters.keySet().iterator()
    var first: Int = 0
    val buffer: StringBuilder = new StringBuilder

    while (iterator.hasNext) {
      val name: String = iterator.next().asInstanceOf[String]
      val value: String = parameters.getProperty(name)

      if (first != 0)
        buffer.append("&")

      buffer.append(name)
      buffer.append("=")
      buffer.append(value)

      first += 1

    }

    buffer.toString()

  }

}
