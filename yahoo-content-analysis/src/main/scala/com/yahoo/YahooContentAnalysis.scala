package com.yahoo

import java.io.{PrintStream, File}
import scala.io.Source
import org.dbpedia.spotlight.utils.HttpRequestUtils._
import org.apache.commons.logging.LogFactory
import org.apache.commons.lang.WordUtils
import net.liftweb.json.JsonParser._
import org.dbpedia.spotlight.bean.DBpediaResource
import org.dbpedia.spotlight.utils.FileUtils._
import scala.collection.mutable.ArrayBuffer


object YahooContentAnalysis {

  implicit val formats = net.liftweb.json.DefaultFormats

  private val LOG = LogFactory.getLog(YahooContentAnalysis.getClass)

  def main(args: Array[String]) {

    if (args.length < 3) {
      println("\nUsage: YahooContentAnalysis <Yahoo Service> <Texts folder>  <Capitalize?>  \n")
      System.exit(1)
    }


    val yahoo_endpoint = args(0)
    val current_dir = args(1)
    val capitalize = args(2).equalsIgnoreCase("capitalize")



    for (file <- new File(current_dir).listFiles) {

      LOG.info("Processing file: %s ...".format(file.getAbsoluteFile))

      val text = (if (capitalize) WordUtils.capitalizeFully(Source.fromFile(file.getAbsoluteFile).mkString) else Source.fromFile(file.getAbsoluteFile).mkString).replace("'","")

      val result = getContent(yahoo_endpoint, getUrlParameters(text, "UTF-8"))

      var buffer = new ArrayBuffer[DBpediaResource]

      val json = parse(result)

      val entities = (json \\ "query" \\ "results" \\ "entities" \\ "entity")

      entities.children.foreach(resource => {

        val contextualScore = (entities \\ "score").extractOrElse("0").toDouble

        val uri = (entities \\ "wiki_url").extractOrElse("").replace("http://en.wikipedia.com/wiki/", "")

        if (!uri.equals("") && buffer.filter(_.getURI.equals(uri)).isEmpty)
          buffer.append(new DBpediaResource(contextualScore, uri))

      })


      val allOut = new PrintStream("../results/yahoo-content-analysis/%s.json".format(file.getName.replace(".txt", "")))

      LOG.info("Writing file: %s ...".format(file.getName.replace(".txt", "")))

      append(allOut, buffer.toList.sortBy(-_.getScore))

      allOut.close()

    }

  }


}
