package org.dbpedia.spotlight.utils

import java.io._
import org.dbpedia.spotlight.bean.DBpediaResource

object FileUtils {


  /**
   * Create a file based on https://github.com/sandroacoelho/automated-audio-tagging-evaluation format
   * @param stream
   * @param tags
   */
  def append(stream: PrintStream, tags: List[(DBpediaResource)]) {
    val template = "{\"score\": %f, \"link\": \"%s\"}"
    stream.append("[" + tags.map(sl => template.format(sl.getScore, sl.getURL)).mkString(", \n") + "]\n")
  }


}