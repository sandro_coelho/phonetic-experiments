package org.dbpedia.spotlight

import java.io.{FileWriter, File}
import scala.io.Source

object FileUtils {

  def writeToFile(nome:String, conteudo:String)= (new FileWriter(nome) {fileWriter => {
    fileWriter.write(conteudo)
    fileWriter.close()
  }})


  def main(args: Array[String]) {


    val current_dir = "/home/scoelho/git/phonetic-experiments/results/dbpedia-spotlight"
    val destination = "/home/scoelho/git/automated-audio-tagging-evaluation/data/automated-tags/"

    for (file <- new File(current_dir).listFiles) {


      writeToFile(destination + file.getName,
        Source.fromFile(file.getAbsolutePath).mkString.replace("\n", "").replace("0,0", "0.0").replace("0,1","0.1").replace("0,2", "0.2").
          replace("0,3", "0.3").replace("0,4", "0.4").replace("0,5", "0.5").replace("0,6", "0.6").
          replace("0,7", "0.7").replace("0,8", "0.8").replace("0,9", "0.9"))

    }
  }
}
