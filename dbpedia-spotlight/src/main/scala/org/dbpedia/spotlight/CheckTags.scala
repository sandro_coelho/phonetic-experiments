package org.dbpedia.spotlight

import scala.io.Source
import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import java.net.URLDecoder

object CheckTags {



  def main(args: Array[String]){

    var buffer   = new ArrayBuffer[String]()
    var bufferTags  = new ArrayBuffer[String]()

    Source.fromFile("/home/scoelho/temp/occs.bbc.uriSorted.tsv").getLines().foreach(linha=> {
      buffer += linha.split("\t")(1)
    })

    val wikipedia_uris = buffer.toSet

    Source.fromFile("/home/scoelho/BBC/tags.tsv").getLines().foreach(linhaTags=>{

      bufferTags +=(URLDecoder.decode(linhaTags.split("\t")(1).replace("http://dbpedia.org/resource/", ""), "UTF-8"))
    })

    val bbc_uris = bufferTags.toSet

    val naoMapeado = bbc_uris.filterNot(wikipedia_uris)

    naoMapeado.foreach(tag=> println(tag))






  }

}
