package org.dbpedia.spotlight

import java.io.{File, PrintStream}
import org.dbpedia.spotlight.bean.DBpediaResource
import scala.io.Source
import org.dbpedia.spotlight.utils.DBpediaSpotlightUtils._
import net.liftweb.json.JsonParser._
import scala.collection.mutable.ArrayBuffer
import org.dbpedia.spotlight.utils.FileUtils._
import org.apache.commons.logging.LogFactory
import org.apache.commons.lang.WordUtils


object Spotlight {

  implicit val formats = net.liftweb.json.DefaultFormats

  private val LOG = LogFactory.getLog(Spotlight.getClass)


  def proccessText(spotlight_endpoint:String, text: String, buffer: ArrayBuffer[DBpediaResource], dictionary:Array[String]): ArrayBuffer[DBpediaResource] = {

    var json = parse(getContent(spotlight_endpoint, getUrlParameters(text, "UTF-8")))

    var resources = json \\ "Resources"

    //Hack
    while (resources.children.length ==0 )
    {
      json = parse(getContent(spotlight_endpoint, getUrlParameters(text, "UTF-8")))
      resources = json \\ "Resources"
      println(resources.children.length)

    }

    resources.children.foreach(resource => {

      val uri = if ((resource \\ "@URI").children.isEmpty) (resource \\ "@URI").extractOrElse[String]("") else (resource \\ "@URI").children.head.extractOrElse[String]("")

      val contextualScore = if ((resource \\ "@similarityScore").children.isEmpty) (resource \\ "@similarityScore").extractOrElse[String]("0").toDouble else ((resource \\ "@similarityScore").children.head.extractOrElse[String]("0")).toDouble

      if (!uri.equals("") && buffer.filter(_.getURI.equals(uri)).isEmpty & dictionary.contains(uri))
        buffer.append(new DBpediaResource(contextualScore, uri))
    })

    buffer

  }

  def main(args: Array[String]) {


    if (args.length < 4) {
      println("\nUsage: Spotlight <Spotlight Service> <Texts folder>  <Capitalize?> <dictionar>y  \n")
      System.exit(1)
    }


    val spotlight_endpoint = args(0)
    val current_dir = args(1)
    val capitalize = args(2).equalsIgnoreCase("capitalize")
    val dictionary = Source.fromFile(args(3), "UTF-8").getLines().mkString("\n").split("\n")

    for (file <- new File(current_dir).listFiles) {

      LOG.info("Processing file: %s ...".format(file.getAbsoluteFile))

      val text = Source.fromFile(file.getAbsoluteFile).mkString
      val capitalizedText = if (capitalize) WordUtils.capitalizeFully(text) else ""

      var buffer = new ArrayBuffer[DBpediaResource]

      buffer = proccessText(spotlight_endpoint,text, buffer, dictionary)

      if (capitalize)
        buffer = proccessText(spotlight_endpoint,capitalizedText, buffer, dictionary)

      val allOut = new PrintStream("../results/dbpedia-spotlight/%s.json".format(file.getName.replace(".txt", "")))

      LOG.info("Writing file: %s ...".format(file.getName.replace(".txt", "")))

      append(allOut, buffer.toList.sortBy(-_.getScore))

      allOut.close()

    }

  }


}
