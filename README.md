phonetic-experiments
====================

Spotlight Phonetic Experiments


Usage:

**DBpedia Spotlight**

     export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF-8
  
     mvn scala:run -Dlauncher=Spotlight
     

**Folder**: dbpedia-spotlight

**Output Results**: results/dbpedia-spotlight

-------------------------------------------------------


**Yahoo Content Analysis**

     export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF-8

     mvn scala:run -Dlauncher=YahooContentAnalysis



**Folder**: yahoo-content-analysis

**Output Results**: results/yahoo-content-analysis

-------------------------------------------------------
**NER Standford**

     ./stanford-ner.sh
     
     mvn scala:run -Dlauncher=Converter
   
**Folder**: ner

**Output Results**: results/ner

-------------------------------------------------------

Usage:

**DBpedia Spotlight Phonetic**

     export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF-8
  
     mvn scala:run -Dlauncher=Spotlight
     

**Folder**: dbpedia-spotlight

**Output Results**: results/phonetic-dbpedia-spotlight


