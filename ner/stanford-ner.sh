#!/bin/bash

FOLDER="../texts/"

FILES_LIST=$(find $FOLDER -type f)
for FILE in $FILES_LIST;
do
   
   FILENAME=$(echo $FILE | cut -c10-17)
   ./stanford-ner-2013-06-20/ner.sh $FILE > "../results/ner/$FILENAME.log"


done

